/*
 * TechAssignment
 *
 * TechAssignment is a simple service
 *
 * API version: 1.0.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func HealthGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Health{Message: "OK"})

}

func MetricsGet(w http.ResponseWriter, r *http.Request) {
	//My understanding is that this endpoint should not return JSON data but just a descriptive string
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	var responseSum int64 = 0
	for _, resp := range metrics.ResponseTimes {
		responseSum += resp.Microseconds()
	}
	var responseAverage int64
	if len(metrics.ResponseTimes) == 0 {
		responseAverage = 0
	} else {
		responseAverage = responseSum / int64(len(metrics.ResponseTimes))
	}
	//assuming it's safe to cast int to int64
	w.Write([]byte(fmt.Sprintf("Success: %v. Failures: %v. Avg. resp. time: %vµs", metrics.Success, metrics.Failure, responseAverage)))
	// json.NewEncoder(w).Encode(metrics)
}

func ReadyGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Ready{Message: "OK"})
}

func TechPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var techProcess TechProcess
	err := json.NewDecoder(r.Body).Decode(&techProcess)
	errorDetail := ""
	if err != nil {
		errorDetail = "Bad Request, the payload is invalid."
	}
	//There probably a better way to validate input, this looks a bit messy
	//Name string, Type_ string, and Tags [] are all required fields. Additionally, Name has a max length of 56 and Type_ only has 3 allowed values.
	if len(techProcess.Name) == 0 {
		errorDetail = "Missing field 'name'"
	}
	if len(techProcess.Name) > 56 {
		errorDetail = "Field 'name' has a max length of 56"
	}
	if len(techProcess.Type_) == 0 {
		errorDetail = "Missing field 'type'"
	}
	if techProcess.Type_ != "qlikview" && techProcess.Type_ != "qlicksense" && techProcess.Type_ != "qcs" {
		errorDetail = "Field 'type' most be  one of the following: 'qlikview', 'qliksense' or 'qcs'"
	}
	if len(techProcess.Tags) == 0 {
		errorDetail = "Missing field 'tags'"
	}
	if errorDetail != "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ModelError{Title: "Error", Code: fmt.Sprintf("%d", http.StatusBadRequest), Detail: errorDetail})
		return
	}
	w.WriteHeader(http.StatusCreated)
	//assume id would come from database
	json.NewEncoder(w).Encode(Tech{Id: "x", Name: techProcess.Name, Type_: techProcess.Type_, Tags: techProcess.Tags, NumberOfTags: len(techProcess.Tags)})
}
