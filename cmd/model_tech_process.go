/*
 * TechAssignment
 *
 * TechAssignment is a simple service
 *
 * API version: 1.0.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type TechProcess struct {
	// The name of the TechProcess
	Name string `json:"name"`

	Type_ string `json:"type"`

	Tags []string `json:"tags"`
}
