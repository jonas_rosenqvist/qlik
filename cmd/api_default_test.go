package swagger

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHealth(t *testing.T) {
	req, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(HealthGet)
	handler.ServeHTTP(rr, req)
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Expected status code %v but got %v",
			http.StatusOK, status)
	}
	msg := []byte(rr.Body.String())
	var health Health
	json.Unmarshal(msg, &health)
	if health.Message != "OK" {
		t.Errorf(`Expected health.Message to equal "OK", but got %v`, health.Message)
	}
}

func TestReady(t *testing.T) {
	req, err := http.NewRequest("GET", "/ready", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(HealthGet)
	handler.ServeHTTP(rr, req)
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Expected status code %v but got %v",
			http.StatusOK, status)
	}
	msg := []byte(rr.Body.String())
	var ready Ready
	json.Unmarshal(msg, &ready)
	if ready.Message != "OK" {
		t.Errorf(`Expected ready.Message to equal "OK", but got %v`, ready.Message)
	}

}

func TestTech(t *testing.T) {

	var jsonStr = []byte(`{"name":"Test", "type": "qlikview", "tags": ["tag1", "tag"]}`)
	req, err := http.NewRequest("POST", "/tech", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(TechPost)
	handler.ServeHTTP(rr, req)
	status := rr.Code
	if status != http.StatusCreated {
		t.Errorf("Expected status code %v but got %v",
			http.StatusCreated, status)
	}
	msg := []byte(rr.Body.String())
	var tech Tech
	json.Unmarshal(msg, &tech)
	if tech.Id != "x" {
		t.Errorf(`Expected tech.Id to equal "x", but got %v`, tech.Id)
	}
	if tech.Name != "Test" {
		t.Errorf(`Expected tech.Name to equal "Test", but got %v`, tech.Name)
	}
	if tech.Type_ != "qlikview" {
		t.Errorf(`Expected tech.Type_ to equal "qlikview", but got %v`, tech.Type_)
	}
}
