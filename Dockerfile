FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
# download modules to local cache
RUN go mod download

# copy source code
COPY main.go ./
COPY cmd ./cmd

# build executable
RUN go build -o /qlik-tech-assignment
# Run executable
CMD [ "/qlik-tech-assignment" ]