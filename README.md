# Qlik TechAssignment

## Plan
This plan is heavily affected by the fact that I have no prior experience in golang.  Because of this, I chose to start by reading up on the official documentation and going over some beginners tutorials. As I learn the basics, I start looking into the assignment to see how I can approach a solution. The swagger service side code gen provides a good foundation for the task to build upon. Best solutions for remaining issues, including solving the logging and handling the request count, is determinted by looking up support for various patterns and techniches (e.g. middleware, annotations).

## Risks
Due to my lack of experience with golang, there's a very high risk of me writing code that is not idiomatic golang and not following best practises, and well has being of inferior quality overall. These risks can hopefully be somewhat mitigated by relying of my prior experience of building APIs, and incorporating applicable knowledge in this project.

## Getting started 
Requires go. Tested with version 1.17.7
From the root directory:
- Running locally: `go run main.go`
- Building and running with docker: `docker build . -t <tag>` followed by `docker run -p 8080:8080 <tag>`
- Running tests: ` go test ./...`